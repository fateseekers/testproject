import React from 'react';
import Cards from "./components/Card";

import './App.css';
import './assets/svg/background.svg'

function App() {
  const feeds = [
      {
          id: 1,
          filling: "с фуа-гра",
          portions: 10,
          additions: [
              'мышь в подарок'
          ],
          weight: "0,5",
          status: "default",
          selectedText: "Печень утки разварная с артишоками."
      },
      {
          id: 2,
          filling: "с рыбой",
          portions: 40,
          additions: [
              '2 мыши в подарок'
          ],
          weight: "2",
          status: "selected",
          selectedText: "Головы щучьи с чесноком да свежайшая сёмгушка."
      },
      {
          id: 3,
          filling: "с курой",
          portions: 100,
          additions: [
              '5 мышей в подарок',
              'заказчик доволен'
          ],
          weight: "5",
          status: "disabled",
          selectedText: "Филе из цыплят с трюфелями в бульоне."
      },
  ];
  return (
      <div className={'container'}>
          <div className={'container__block'}>
              <h1 className={'container__heading'}>Ты сегодня покормил кота?</h1>
                <div className={'cards'}>
                    {feeds.map((feed) => {
                        return <Cards key={feed.id} feed={feed}/>
                    })}
                </div>
          </div>
      </div>
  );
}

export default App;
