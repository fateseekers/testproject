import React, {useState} from 'react'
import '../assets/jpg/cat.jpg'

function Cards({feed}) {
    const [status, setStatus] = useState(feed.status);

    function onClick(){
        if(status !== "disabled"){
            if(status === "default"){
                setStatus("selected")
            } else {
                setStatus("default")
            }
        }
    }

    function onMouseOut(){
        if(status !== "disabled"){
            if(status === "selected"){
                setStatus("selectedhover")
            } else {
                setStatus("defaulthover")
            }
        }
    }

    function onMouseIn(){
        if(status !== "disabled"){
            if(status === "selectedhover" || status === "selected"){
                setStatus("selected")
            } else {
                setStatus("default")
            }
        }
    }

    return (
        <div className={`card__block ${status !== 'default' ? status : ''}`}>
            <div className={'card__block--container'} onClick={() => onClick()} onMouseLeave={() => onMouseOut()} onMouseOver={() => onMouseIn()}>
                <div className={'card__block--content'}>
                    <div className={'card__block--description'}>
                        <p className={'card__block--text'}>
                            {status === "selectedhover"
                                ? "Котэ не одобряет?"
                                : "Сказочное заморское яство"
                            }
                        </p>
                        <h1 className={'card__block--name'}>Нямушка</h1>
                        <p className={'card__block--filling'}>{feed.filling}</p>
                        <ul className={'card__block--list'}>
                            <li className={'card__block--item'}>
                                {feed.portions} порций
                            </li>
                            {feed.additions.map((addition, index) => {
                                return (
                                    <li key={index} className={'card__block--item'}>
                                        {addition}
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                    <div className={'card__block--weight'}>
                        <span className={'card__block--weightcount'}>{feed.weight}</span>
                        <p className={'card__block--weighttext'}>кг</p>
                    </div>
                </div>
            </div>
            {status === "disabled"
                ? <p className={'card__block--action'}>Печалька {feed.filling} закончился</p>
                : status === "selected" || status === "selectedhover"
                    ? <p className={'card__block--action'}>{feed.selectedText}</p>
                    : <p className={'card__block--action'}>Чего сидишь? Порадуй котэ, <span className={'card--block--link dashed'} onClick={onClick}>купи</span><span className={'card--block--link'}>.</span></p>
            }
        </div>
    )
}

export default Cards
